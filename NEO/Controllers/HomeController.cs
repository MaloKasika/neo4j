﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DBase;
using DBase.DBModels;

namespace NEO.Controllers
{
    public class HomeController : Controller
    {
   
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public void SaveToRedis(string dataResult, string termin)
        {
            CommunicationRedis.SaveInRedis(termin, dataResult);
        }

        public ActionResult Search(string term)
        {
            List<String> redisRes = CommunicationRedis.GetFromRedis(term);

            List<String> listS = new List<String>();

            if (redisRes.Count!=0)
            {
                listS = redisRes;

            }

            List<string> terms = term.Split(' ').ToList();

            List<NBAPlayer> playersToShow = new List<NBAPlayer>();
            List<NBAPlayer> lastnamesToShow = new List<NBAPlayer>();
            List<Team> teamsToShow = new List<Team>();
            List<NBACoach> coachesToShow = new List<NBACoach>();
            List<Position> positionToShow = new List<Position>();
            List<Division> divisionToShow = new List<Division>();
            List<Conference> conferenceToShow = new List<Conference>();

            foreach (string singleTerm in terms)
            {
                if (singleTerm.Equals(""))
                    continue;
                playersToShow.AddRange(Communication.GetNBAPlayers(singleTerm).Where(x=>playersToShow.FirstOrDefault(y=>y.ID==x.ID)==null).ToList());
                lastnamesToShow.AddRange(Communication.GetLastnames(singleTerm).Where(x => lastnamesToShow.FirstOrDefault(y => y.ID == x.ID) == null).ToList());
                teamsToShow.AddRange(Communication.GetTeams(singleTerm).Where(x => teamsToShow.FirstOrDefault(y => y.ID == x.ID) == null).ToList());
                coachesToShow.AddRange(Communication.GetNBACoaches(singleTerm).Where(x => coachesToShow.FirstOrDefault(y => y.ID == x.ID) == null).ToList());
                positionToShow.AddRange(Communication.GetPositions(singleTerm).Where(x => positionToShow.FirstOrDefault(y => y.ID == x.ID) == null).ToList());
                divisionToShow.AddRange(Communication.GetDivisions(singleTerm).Where(x => divisionToShow.FirstOrDefault(y => y.ID == x.ID) == null).ToList());
                conferenceToShow.AddRange(Communication.GetConferences(singleTerm).Where(x => conferenceToShow.FirstOrDefault(y => y.ID == x.ID) == null).ToList());
            }
            

            if (playersToShow.Count > 0)
            {
                List<String> pom = new List<string>();
                pom.AddRange(playersToShow.Select(x => x.ToString()).ToList());
                listS.AddRange(pom.Where(x => listS.Contains(x) == false).ToList());
            }

            if (lastnamesToShow.Count > 0)
            {
                List<String> pom = new List<string>();
                pom.AddRange(lastnamesToShow.Select(x => x.ToString()).ToList());
                listS.AddRange(pom.Where(x => listS.Contains(x) == false).ToList());
            }

            if (teamsToShow.Count > 0)
            {
                List<String> pom = new List<string>();
                pom.AddRange(teamsToShow.Select(x => x.ToString()).ToList());
                listS.AddRange(pom.Where(x => listS.Contains(x) == false).ToList());
            }

            if(coachesToShow.Count > 0)
            {
                List<String> pom = new List<string>();
                pom.AddRange(coachesToShow.Select(x => x.ToString()).ToList());
                listS.AddRange(pom.Where(x => listS.Contains(x) == false).ToList());
            }

            if (positionToShow.Count > 0)
            {
                List<String> pom = new List<string>();
                pom.AddRange(positionToShow.Select(x => x.ToString()).ToList());
                listS.AddRange(pom.Where(x => listS.Contains(x) == false).ToList());
            }

            if (divisionToShow.Count > 0)
            {
                List<String> pom = new List<string>();
                pom.AddRange(divisionToShow.Select(x => x.ToString()).ToList());
                listS.AddRange(pom.Where(x => listS.Contains(x) == false).ToList());
            }

            if (coachesToShow.Count > 0)
            {
                List<String> pom = new List<string>();
                pom.AddRange(conferenceToShow.Select(x => x.ToString()).ToList());
                listS.AddRange(pom.Where(x => listS.Contains(x) == false).ToList());
            }
            

            JsonResult result = new JsonResult { Data=listS, JsonRequestBehavior=JsonRequestBehavior.AllowGet };
            return result;
        }
        
    }
}