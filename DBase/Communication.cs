﻿using Neo4jClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBase.DBModels;
using Neo4jClient.Cypher;

namespace DBase
{
    public static class Communication
    {
        private static GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "napredne");

        public static List<NBAPlayer> GetNBAPlayers(string term)
        {
            client.Connect();

            List<NBAPlayer> foundPlayers = new List<NBAPlayer>();

            var query = new Neo4jClient.Cypher.CypherQuery("match (n:NBAPlayer) where toLower(n.name)=~'.*" + term.ToLower()+ ".*' or toLower(n.age)=~'.*" + term.ToLower()+ ".*' or toLower(n.height)=~'.*" + term.ToLower()+ ".*' or toLower(n.weight)=~'.*" + term.ToLower() + ".*' return n",
                                                  new Dictionary<string, object>(), CypherResultMode.Set);

            foundPlayers = ((IRawGraphClient)client).ExecuteGetCypherResults<NBAPlayer>(query).ToList();

            foreach(NBAPlayer player in foundPlayers)
            {
                var queryID = new Neo4jClient.Cypher.CypherQuery("match (n:NBAPlayer) where n.name=~'.*" + player.name + ".*' and n.age=~'.*" + player.age + ".*' and n.height=~'.*" + player.height + ".*' and n.weight=~'.*" + player.weight + ".*' return ID(n)",
                       new Dictionary<string, object>(), CypherResultMode.Set);

                player.ID = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryID).ToList().FirstOrDefault();

                foreach (String conn in player.connections)
                {
                    var queryLN = new Neo4jClient.Cypher.CypherQuery("match (n:NBAPlayer)-["+conn+"]-(ret) where n.name=~'.*" + player.name + ".*' and n.age=~'.*" + player.age + ".*' and n.height=~'.*" + player.height + ".*' and n.weight=~'.*" + player.weight + ".*' return ret",
                                           new Dictionary<string, object>(), CypherResultMode.Set);

                    switch (conn)
                    {
                        case ":NAMES":
                                List<Lastname> lname = ((IRawGraphClient)client).ExecuteGetCypherResults<Lastname>(queryLN).ToList();
                                player.lastname = lname[0].lastName;
                                break;

                        case ":PLAYSIN":
                            List<Team> teams = ((IRawGraphClient)client).ExecuteGetCypherResults<Team>(queryLN).ToList();
                            foreach(Team t in teams)
                            {
                                var queryIDT = new Neo4jClient.Cypher.CypherQuery("match (n:Team) where n.name=~'.*" + t.name + ".*' and n.city=~'.*" + t.city + ".*' and n.abb=~'.*" + t.abb + ".*' return ID(n)",
                                           new Dictionary<string, object>(), CypherResultMode.Set);
                                t.ID = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryIDT).ToList().FirstOrDefault();
                            }

                            player.team = teams[0];
                            break;

                        case ":PLAYSAT":
                            List<Position> positions = ((IRawGraphClient)client).ExecuteGetCypherResults<Position>(queryLN).ToList();
                            foreach (Position t in positions)
                            {
                                var queryIDP = new Neo4jClient.Cypher.CypherQuery("match (n:Position) where n.position=~'.*" + t.position + ".*' and n.abb=~'.*" + t.abb+ ".*' return ID(n)",
                                           new Dictionary<string, object>(), CypherResultMode.Set);
                                t.ID = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryIDP).ToList().FirstOrDefault();
                            }
                            player.position = positions[0];
                            break;
                    }
                }
            }

            return foundPlayers;
        }

        public static List<NBAPlayer> GetLastnames(string term)
        {
            client.Connect();

            List<NBAPlayer> found = new List<NBAPlayer>();

            var query = new Neo4jClient.Cypher.CypherQuery("match (ln:LastName)-[:NAMES]-(p) where toLower(ln.lastName)=~'.*" + term.ToLower() + ".*' return p",
                                                  new Dictionary<string, object>(), CypherResultMode.Set);

            found = ((IRawGraphClient)client).ExecuteGetCypherResults<NBAPlayer>(query).ToList();

            foreach (NBAPlayer player in found)
            {
                var queryID = new Neo4jClient.Cypher.CypherQuery("match (n:NBAPlayer) where n.name=~'.*" + player.name + ".*' and n.age=~'.*" + player.age + ".*' and n.height=~'.*" + player.height + ".*' and n.weight=~'.*" + player.weight + ".*' return ID(n)",
                       new Dictionary<string, object>(), CypherResultMode.Set);

                player.ID = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryID).ToList().FirstOrDefault();

                foreach (String conn in player.connections)
                {
                    var queryLN = new Neo4jClient.Cypher.CypherQuery("match (n:NBAPlayer)-[" + conn + "]-(ret) where n.name=~'.*" + player.name + ".*' and n.age=~'.*" + player.age + ".*' and n.height=~'.*" + player.height + ".*' and n.weight=~'.*" + player.weight + ".*' return ret",
                                           new Dictionary<string, object>(), CypherResultMode.Set);

                    switch (conn)
                    {
                        case ":NAMES":
                            List<Lastname> lname = ((IRawGraphClient)client).ExecuteGetCypherResults<Lastname>(queryLN).ToList();
                            player.lastname = lname[0].lastName;
                            break;

                        case ":PLAYSIN":
                            List<Team> teams = ((IRawGraphClient)client).ExecuteGetCypherResults<Team>(queryLN).ToList();
                            foreach (Team t in teams)
                            {
                                var queryIDT = new Neo4jClient.Cypher.CypherQuery("match (n:Team) where n.name=~'.*" + t.name + ".*' and n.city=~'.*" + t.city + ".*' and n.abb=~'.*" + t.abb + ".*' return ID(n)",
                                           new Dictionary<string, object>(), CypherResultMode.Set);
                                t.ID = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryIDT).ToList().FirstOrDefault();
                            }
                            player.team = teams[0];
                            break;

                        case ":PLAYSAT":
                            List<Position> positions = ((IRawGraphClient)client).ExecuteGetCypherResults<Position>(queryLN).ToList();
                            foreach (Position t in positions)
                            {
                                var queryIDP = new Neo4jClient.Cypher.CypherQuery("match (n:Position) where n.position=~'.*" + t.position + ".*' and n.abb=~'.*" + t.abb + ".*' return ID(n)",
                                           new Dictionary<string, object>(), CypherResultMode.Set);
                                t.ID = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryIDP).ToList().FirstOrDefault();
                            }
                            player.position = positions[0];
                            break;
                    }
                }
            }

            return found;
        }

        public static List<Team> GetTeams(string term)
        {
            client.Connect();

            List<Team> foundTeams = new List<Team>();

            var query = new Neo4jClient.Cypher.CypherQuery("match (t:Team) where toLower(t.abb)=~'.*" + term.ToLower() + ".*' or toLower(t.city)=~'.*" + term.ToLower()+ ".*' or toLower(t.name)=~'.*" + term.ToLower()+".*' return t",
                                                  new Dictionary<string, object>(), CypherResultMode.Set);

            foundTeams = ((IRawGraphClient)client).ExecuteGetCypherResults<Team>(query).ToList();

            foreach(Team team in foundTeams)
            {
                var queryIDT = new Neo4jClient.Cypher.CypherQuery("match (n:Team) where n.name=~'.*" + team.name + ".*' and n.city=~'.*" + team.city + ".*' and n.abb=~'.*" + team.abb + ".*' return ID(n)",
                                       new Dictionary<string, object>(), CypherResultMode.Set);
                team.ID = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryIDT).ToList().FirstOrDefault();

                foreach (String conn in team.connections)
                {
                    var queryLN = new Neo4jClient.Cypher.CypherQuery("match (n:Team)-[" + conn + "]-(ret) where n.name=~'.*" + team.name + ".*' return ret",
                                           new Dictionary<string, object>(), CypherResultMode.Set);

                    switch (conn)
                    {
                        case ":BELONGS":
                            List<Division> divisions = ((IRawGraphClient)client).ExecuteGetCypherResults<Division>(queryLN).ToList();
                            var queryIDD = new Neo4jClient.Cypher.CypherQuery("match (n:Division) where n.position=~'.*" + divisions[0].name + ".*' return ID(n)",
                                           new Dictionary<string, object>(), CypherResultMode.Set);
                            divisions[0].ID = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryIDD).ToList().FirstOrDefault();                            
                            team.division = divisions[0];
                            break;

                        case ":COACHES":
                            List<NBACoach> coaches = ((IRawGraphClient)client).ExecuteGetCypherResults<NBACoach>(queryLN).ToList();
                            var queryIDC = new Neo4jClient.Cypher.CypherQuery("match (n:NBACoach) where n.name=~\".*" + coaches[0].name + ".*\" return ID(n)",
                                           new Dictionary<string, object>(), CypherResultMode.Set);
                            coaches[0].ID = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryIDC).ToList().FirstOrDefault();
                            team.coach = coaches[0];
                            break;

                        case ":PLAYSIN":
                            List<NBAPlayer> players = ((IRawGraphClient)client).ExecuteGetCypherResults<NBAPlayer>(queryLN).ToList();

                            foreach (NBAPlayer pl in players)
                            {
                                var queryPL = new Neo4jClient.Cypher.CypherQuery("match (n:NBAPlayer)-[:NAMES]-(ret) where n.name=~'.*" + pl.name + ".*' return ret",
                                   new Dictionary<string, object>(), CypherResultMode.Set);

                                List<Lastname> ln = ((IRawGraphClient)client).ExecuteGetCypherResults<Lastname>(queryPL).ToList();

                                var queryID = new Neo4jClient.Cypher.CypherQuery("match (n:NBAPlayer) where n.name=~'.*" + pl.name + ".*' and n.age=~'.*" + pl.age + ".*' and n.height=~'.*" + pl.height + ".*' and n.weight=~'.*" + pl.weight + ".*' return ID(n)",
                                              new Dictionary<string, object>(), CypherResultMode.Set);

                                pl.ID = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryID).ToList().FirstOrDefault();
                                
                                pl.lastname = ln[0].lastName;
                            }
                            
                            team.players = players;
                            break;
                    }
                }
            }

            return foundTeams;
        }

        public static List<NBACoach> GetNBACoaches(string term)
        {
            client.Connect();

            List<NBACoach> foundCoaches = new List<NBACoach>();

            var query = new Neo4jClient.Cypher.CypherQuery("match (c:NBACoach) where toLower(c.name)=~'.*" + term.ToLower() + ".*' or toLower(c.loses)=~'.*" + term.ToLower() + ".*' or toLower(c.wins)=~'.*" + term.ToLower() + ".*' return c",
                                                  new Dictionary<string, object>(), CypherResultMode.Set);

            foundCoaches = ((IRawGraphClient)client).ExecuteGetCypherResults<NBACoach>(query).ToList();

            foreach(NBACoach coach in foundCoaches)
            {
                var queryID = new Neo4jClient.Cypher.CypherQuery("match (n:NBACoach) where n.name=~\".*" + coach.name + ".*\" return ID(n)",
                      new Dictionary<string, object>(), CypherResultMode.Set);

                coach.ID = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryID).ToList().FirstOrDefault();

                var queryLN = new Neo4jClient.Cypher.CypherQuery("match (n:NBACoach)-[:COACHES]-(ret) where n.name=~\".*" + coach.name + ".*\" return ret",
                
                                  new Dictionary<string, object>(), CypherResultMode.Set);

                List<Team> teams = ((IRawGraphClient)client).ExecuteGetCypherResults<Team>(queryLN).ToList();

                foreach (Team t in teams)
                {
                    var queryIDT = new Neo4jClient.Cypher.CypherQuery("match (n:Team) where n.name=~'.*" + t.name + ".*' and n.city=~'.*" + t.city + ".*' and n.abb=~'.*" + t.abb + ".*' return ID(n)",
                               new Dictionary<string, object>(), CypherResultMode.Set);
                    t.ID = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryIDT).ToList().FirstOrDefault();
                }

                coach.team = teams[0];
            }
            
            return foundCoaches;
        }

        public static List<Conference> GetConferences(string term)
        {
            client.Connect();

            List<Conference> foundConferences = new List<Conference>();

            var query = new Neo4jClient.Cypher.CypherQuery("match (c:Conference) where toLower(c.coast)=~'.*" + term.ToLower() + ".*' return c",
                                                  new Dictionary<string, object>(), CypherResultMode.Set);

            foundConferences = ((IRawGraphClient)client).ExecuteGetCypherResults<Conference>(query).ToList();

            foreach(Conference conf in foundConferences)
            {
                var queryID = new Neo4jClient.Cypher.CypherQuery("match (c:Conference) where c.coast=~'.*" + conf.coast + ".*' return ID(c)",
                                                  new Dictionary<string, object>(), CypherResultMode.Set);

                conf.ID = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryID).ToList().FirstOrDefault();
                
                var queryLN = new Neo4jClient.Cypher.CypherQuery("match (n:Conference)-[:PARTOF]-(ret) where n.coast=~'.*" + conf.coast + ".*' return ret",
                                  new Dictionary<string, object>(), CypherResultMode.Set);

                List<Division> divisions = ((IRawGraphClient)client).ExecuteGetCypherResults<Division>(queryLN).ToList();

                foreach (Division t in divisions)
                {
                    var queryIDT = new Neo4jClient.Cypher.CypherQuery("match (n:Division) where n.name=~'.*" + t.name + ".*' return ID(n)",
                               new Dictionary<string, object>(), CypherResultMode.Set);
                    t.ID = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryIDT).ToList().FirstOrDefault();
                }

                conf.divisions = divisions;
            }

            return foundConferences;
        }

        public static List<Division> GetDivisions(string term)
        {
            client.Connect();

            List<Division> foundDivisions = new List<Division>();

            var query = new Neo4jClient.Cypher.CypherQuery("match (d:Division) where toLower(d.name)=~'.*" + term.ToLower() + ".*' return d",
                                                  new Dictionary<string, object>(), CypherResultMode.Set);

            foundDivisions = ((IRawGraphClient)client).ExecuteGetCypherResults<Division>(query).ToList();

            foreach(Division div in foundDivisions)
            {
                var queryID = new Neo4jClient.Cypher.CypherQuery("match (c:Division) where c.name=~'.*" + div.name + ".*' return ID(c)",
                                                  new Dictionary<string, object>(), CypherResultMode.Set);

                div.ID = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryID).ToList().FirstOrDefault();

                foreach (String conn in div.connections)
                {
                    var queryLN = new Neo4jClient.Cypher.CypherQuery("match (n:Division)-[" + conn + "]-(ret) where n.name=~'.*" + div.name + ".*' return ret",
                                           new Dictionary<string, object>(), CypherResultMode.Set);

                    switch (conn)
                    {
                        case ":BELONGS":
                            List<Team> teams= ((IRawGraphClient)client).ExecuteGetCypherResults<Team>(queryLN).ToList();
                            foreach (Team t in teams)
                            {
                                var queryIDT = new Neo4jClient.Cypher.CypherQuery("match (n:Team) where n.name=~'.*" + t.name + ".*' and n.city=~'.*" + t.city + ".*' and n.abb=~'.*" + t.abb + ".*' return ID(n)",
                                           new Dictionary<string, object>(), CypherResultMode.Set);
                                t.ID = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryIDT).ToList().FirstOrDefault();
                            }
                            div.teams = teams;
                            break;

                        case ":PARTOF":
                            List<Conference> conferences = ((IRawGraphClient)client).ExecuteGetCypherResults<Conference>(queryLN).ToList();
                            div.conference = conferences[0].coast;
                            break;
                    }
                }
            }

            return foundDivisions;
        }

        public static List<Position> GetPositions(string term)
        {
            client.Connect();

            List<Position> foundPositions = new List<Position>();

            var query = new Neo4jClient.Cypher.CypherQuery("match (p:Position) where toLower(p.abb)=~'.*" + term.ToLower() + ".*' or toLower(p.position)=~'.*" + term.ToLower() + ".*' return p",
                                                  new Dictionary<string, object>(), CypherResultMode.Set);

            foundPositions = ((IRawGraphClient)client).ExecuteGetCypherResults<Position>(query).ToList();

            foreach(Position pos in foundPositions)
            {
                var queryIDP = new Neo4jClient.Cypher.CypherQuery("match (c:Position) where c.position=~'.*" + pos.position+ ".*' return ID(c)",
                                                  new Dictionary<string, object>(), CypherResultMode.Set);

                pos.ID = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryIDP).ToList().FirstOrDefault();
                
                var queryLN = new Neo4jClient.Cypher.CypherQuery("match (n:Position)-[:PLAYSAT]-(ret) where n.position=~'.*" + pos.position + ".*' return ret",
                       new Dictionary<string, object>(), CypherResultMode.Set);
               
                List<NBAPlayer> players = ((IRawGraphClient)client).ExecuteGetCypherResults<NBAPlayer>(queryLN).ToList();

                foreach(NBAPlayer pl in players)
                {
                    var queryID = new Neo4jClient.Cypher.CypherQuery("match (n:NBAPlayer) where n.name=~'.*" + pl.name + ".*' and n.age=~'.*" + pl.age + ".*' and n.height=~'.*" + pl.height + ".*' and n.weight=~'.*" + pl.weight + ".*' return ID(n)",
                                              new Dictionary<string, object>(), CypherResultMode.Set);

                    pl.ID = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryID).ToList().FirstOrDefault();

                    var queryPL = new Neo4jClient.Cypher.CypherQuery("match (n:NBAPlayer)-[:NAMES]-(ret) where n.name=~'.*" + pl.name + ".*' return ret",
                       new Dictionary<string, object>(), CypherResultMode.Set);

                    List<Lastname> ln = ((IRawGraphClient)client).ExecuteGetCypherResults<Lastname>(queryPL).ToList();

                    pl.lastname = ln[0].lastName;
                }

                pos.players = players;
            }
            
            return foundPositions;
        }
    }
}
