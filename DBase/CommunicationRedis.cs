﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Redis;

namespace DBase
{
    public static class CommunicationRedis
    {
        public static void SaveInRedis(string term, string result)
        {
            using (IRedisClient client = new RedisClient())
            {
              //  client.Set("uri:" + term + ":1", result);
                var termList= client.Lists["uri:" + term];
                                
                if (!termList.Contains(result))
                {
                    if (termList.Count == 5)
                        termList.RemoveStart();
                    termList.Add(result);
                }

            };
        }

        public static List<String> GetFromRedis(string term)
        {
            List<String> toReturn = new List<String>();
            using (IRedisClient client = new RedisClient())
            {
                var termList = client.Lists["uri:" + term];

                foreach(var termRes in termList)
                {
                    toReturn.Add(termRes);
                }
            }

            return toReturn;
        }

    }
}
