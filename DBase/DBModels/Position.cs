﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBase.DBModels
{
    public class Position
    {
        public int ID { get; set; }
        public String position { get; set; }
        public String abb { get; set; }

        public List<NBAPlayer> players = new List<NBAPlayer>();

        public override string ToString()
        {
            string ret = position + " also marked as " + abb + ". Some of NBA players that plays at this position are: ";

            for(int i=0;i<players.Count-1;i++)
            {
                ret += players[i].name + " " + players[i].lastname + ", ";
            }

            ret += players.Last().name + " " + players.Last().lastname + ".";

            return ret;
        }
    }
}
