﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBase.DBModels
{
    public class Division
    {
        public int ID { get; set; }
        public String name { get; set; }
        public String conference { get; set; }

        public List<Team> teams = new List<Team>();

        public List<String> connections = new List<string>
        {
            ":BELONGS",
            ":PARTOF"
        };

        public override string ToString()
        {
            string ret = name + " division is a part of " + conference + " conference and has 5 NBA teams, which are: ";

            for(int i=0;i<teams.Count-1;i++)
            {
                ret += teams[i].city + " " + teams[i].name + ", ";
            }

            ret += teams.Last().city + " " + teams.Last().name + ".";
            
            return ret;
        } 
    }
}
