﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBase.DBModels
{
    public class NBAPlayer
    {
        public int ID { get; set; }
        public String name { get; set; }
        public String height { get; set; }
        public String age { get; set; }
        public String weight { get; set; }
        public String lastname { get; set; }
        public Team team { get; set; }
        public Position position { get; set; }

        public List<String> connections = new List<string>
        {
            ":PLAYSIN",
            ":PLAYSAT",
            ":NAMES"
        };

        public override string ToString()
        {
            return "Player " + name + " " + lastname + " is " + age + " years old and he is " + height + " cm tall and weights " + weight + " kg.\n" +
                "He plays " + position.position + " (" + position.abb + ") for " + team.city + " " + team.name + " (" + team.abb + ").";
        }
    }
}
