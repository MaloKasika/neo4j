﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBase.DBModels
{
    public class NBACoach
    {
        public int ID { get; set; }
        public String name { get; set; }
        public String wins { get; set; }
        public String losses { get; set; }
        public Team team { get; set; }
        

        public override string ToString()
        {
            return "Coach " + name + " has a record of " + wins + " wins and " + losses + " losses. " +
                "He is currently a headcoach of "+team.city+" "+team.name+" ("+team.abb+").";
        }
    }
}
