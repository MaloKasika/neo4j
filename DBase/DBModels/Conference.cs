﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBase.DBModels
{
    public class Conference
    {
        public int ID { get; set; }
        public String coast { get; set; }

        public List<Division> divisions = new List<Division>();
        

        public override string ToString()
        {
            string ret= "The " + coast + " conference is located on " + coast.ToLower() + "side of USA and has a 30 NBA teams.\n It consists of 3 divisions: ";

            for(int i=0;i<divisions.Count-1;i++)
            {
                ret += divisions[i].name + ", ";
            }

            ret += divisions.Last().name + ".";
            return ret;
        }
    }
}
