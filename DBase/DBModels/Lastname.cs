﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBase.DBModels
{
    public class Lastname
    {
        public int ID { get; set; }
        public String lastName { get; set; }
        
    }
}
