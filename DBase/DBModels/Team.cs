﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBase.DBModels
{
    public class Team
    {
        public int ID { get; set; }
        public String name { get; set; }
        public String city { get; set; }
        public String abb { get; set; }
        public Division division { get; set; }
        public NBACoach coach { get; set; }

        public List<NBAPlayer> players = new List<NBAPlayer>();
        
        public List<String> connections = new List<string>
        {
            ":PLAYSIN",
            ":COACHES",
            ":BELONGS"
        };

        public override string ToString()
        {
            string ret = "The " + name + " are from " + city + " and also known as " + abb +".\n"+
                "This team belongs to "+division.name+" division. The headcoach is "+coach.name+". " +
                "Some of their players are: ";

            for(int i=0;i<players.Count-1;i++)
            {
                ret += players[i].name + " "+players[i].lastname+ ", ";
            }
            
            ret += players.Last().name + " " + players.Last().lastname + ".";

            return ret;
        }
        
    }
}
